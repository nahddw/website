
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>telei support</title>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        .container {
            width: 80%;
            margin: 10% auto 0;
            /*background-color: #f0f0f0;*/
            padding: 2% 5%;
            border-radius: 10px
        }

        ul {
            padding-left: 20px;
        }

        ul li {
            line-height: 2.3
        }

        a {
            color: #20a53a
        }
        *{
            border:none;
            outline:none;
        }
    </style>
</head>
<body>

<main id="container" role="main" class="container">
    <div class="surface">
        <div class="surface-container">
            <div data-pjax-container class="content">

                <section class="wrapper" tabindex="0">
                    <div class="wrapper-container">

                        <header class="post-header">
                            <h1 itemprop="name headline" class="post-title">
                                <span href="#" itemprop="url" data-pjax title="Telei">Telei</span>
                            </h1>
                        </header>

                        <div itemprop="articleBody" class="post-body">
                            
                            <hr>
                            <p>請盡更新至最新版本</p>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</main>
</body>
</html>